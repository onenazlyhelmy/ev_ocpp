#!/usr/bin/env python3
##install this first
##pip3 install asyncio pathlib websockets==3.4

import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
import asyncio
import websockets
from gpiozero import Button
import time
from random import randrange

port=5660
interval = 1

#Address for the first MCP3008 (0, 0)
mcp0 = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(0, 0))

#Address for the second MCP3008 (0, 1)
mcp1 = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(0, 1))

button1 = Button(23)
button2 = Button(12)
button3 = Button(5)
button4 = Button(6)
button5 = Button(13)
button6 = Button(26)

button7 = Button(24)
button8 = Button(18)
button9 = Button(25)
button10 = Button(22)
button11= Button(27)
button12= Button(17)


def choose():
     #a=randrange(6)
     if button1.is_pressed:
        a=1
     elif button2.is_pressed:
        a=2
     if button3.is_pressed:
        a=3
     if button4.is_pressed:
        a=4
     if button5.is_pressed:
        a=5
     if button6.is_pressed:
        a=6
     return a

def pilih():
     #b=randrange(12)
     if button7.is_pressed:
        b=7
     elif button8.is_pressed:
        b=8
     elif button9.is_pressed:
        b=9
     elif button10.is_pressed:
        b=10
     elif button11.is_pressed:
        b=11
     elif button12.is_pressed:
        b=12
     else :
        b=100

     return b

@asyncio.coroutine
def time(websocket, path):

     try :

         while True:
              a= choose()
              b= pilih()
              print("{0:5} {1:5} {2:5} {3:5} {4:5} {5:5} {6:5} {7:5} {8:5} 0 0  {9:2} {10:2} 1".format(mcp0.read_adc(0),mcp0.read_adc(1),
              mcp0.read_adc(2),mcp0.read_adc(3),mcp0.read_adc(4),mcp0.read_adc(5),mcp0.read_adc(6),mcp0.read_adc(7),mcp1.read_adc(7),
              a,b))
              data= "{0:5},{1:5},{2:5},{3:5},{4:5},{5:5},{6:5},{7:5},{8:5}, 0, 0,{9:2},{10:2}, 1".format(mcp0.read_adc(0),mcp0.read_adc(1),
              mcp0.read_adc(2),mcp0.read_adc(3),mcp0.read_adc(4),mcp0.read_adc(5),mcp0.read_adc(6),mcp0.read_adc(7),mcp1.read_adc(7),
              a,b)
              # print("--sSend data : {} ".format(data))

              # time.sleep(interval)
              yield from websocket.send(data)
              yield from asyncio.sleep(interval)

     except Exception as s:
         print(s)


print("--start websocket --adc--- port : ",port)
start_server = websockets.serve(time, '0.0.0.0', port)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

