# import asyncio
# import websockets
#
# async def hello():
#     uri = "ws://10.222.103.151:5660"
#     async with websockets.connect(uri) as websocket:
#         # name = input("What's your name? ")
#
#         # await websocket.send(name)
#         # print(f"> {name}")
#
#         data = await websocket.recv()
#         print(f"< {data}")
#
# asyncio.get_event_loop().run_until_complete(hello())
# asyncio.get_event_loop().run_forever()


#!/usr/bin/env python

import asyncio
import websockets

url = "ws://10.222.103.151:5660"

@asyncio.coroutine
def hello():
    websocket = yield from websockets.connect(url)
    # name = input("What's your name? ")
    # yield from websocket.send(name)
    # print("> {}".format(name))
    greeting = yield from websocket.recv()
    print("< {}".format(greeting))

asyncio.get_event_loop().run_until_complete(hello())
# asyncio.get_event_loop().run_forever()