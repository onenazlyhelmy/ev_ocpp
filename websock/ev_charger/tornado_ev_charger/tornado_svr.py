import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
from func import *
import json
import uuid

clients = {}
cl_id=0
port=9001
ws_path="/my_ev/pak_hang_mak_ang"


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, application, request, **kwargs):
        super(WSHandler, self).__init__(application, request, **kwargs)

        # print('ip :',self.cl)
        # self.client_id = str(cl_id)
        # cl_id =cl_id +1
        # self.client_id = str(uuid.uuid4())

    def open(self):
        global cl_id

        # print(type(self.request))
        print(self.request)
        # x_real_ip = self.request.headers.get('X-Real-Ip', '')
        print('Client IP:' + self.request.remote_ip)
        if(self.request.remote_ip == '::1' and self.request.host== 'localhost:9001'):
            print('>>> MASTER connected <<<<')
            self.client_id = 'master'
            print('connection id [ {0} ]opened...'.format(self.client_id))
            clients[self.client_id] = self
        else:
            print('>>>>>> slaves connected ')
            self.client_id = str(cl_id)
            print('connection id [ {} ] opened...'.format(self.client_id))
            clients[self.client_id] = self
            cl_id = cl_id + 1
        # self.write_message("The server says: 'Hello'. Connection was accepted.")

    def on_message(self, message):
        self.write_message("The server says: " + message + " back at you")
        #print(self.request)
        if(self.client_id =='master'):

          dat_json = json.loads(message)
          ret = dataproc_cmd(dat_json['cmd_req'])
          # {"cmd_req": "remotestart", "idToken": "r00t1234567890!", "dtime": "20190524074340101"}
          # print('--------------------------------------')
          # print(self.request)
          # print('--------------------------------------')
          # print('----- from client id :', self.client_id)
          print('received from MASTER :', message)
          print('result MASTER :', ret)
          cid = dat_json['cid']
          print('cid :', dat_json['cid'])
          if(ret == 'none'):
              clients[self.client_id] = self
              clients[self.client_id].write_message('{"status":"command not recognize"}')

          else:
              print(' ---------- remote start EV CHARGER------------->')
              # self.client_id = 0
              # clients['0'] = self
              clients[cid].write_message(ret)

        else:
          ret= dataproc(message)
          # print('----- from client id :', self.client_id)
          print('>> received from slave <<  :', message)
          print('>> result <<  :', ret)
          clients[self.client_id] = self
          clients[self.client_id].write_message(ret)

    def on_close(self):
        clients.pop(self.client_id, None)
        print('connection closed...')

application = tornado.web.Application([
  (r'/my_ev/pak_hang_mak_ang', WSHandler),
  (r'/', MainHandler),
  (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])

if __name__ == "__main__":
  print('--tornado start -- on port:',port)
  application.listen(port)
  tornado.ioloop.IOLoop.instance().start()