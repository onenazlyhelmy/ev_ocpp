from flask import Flask, request, render_template
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket import WebSocketServer, WebSocketApplication, Resource
import os

import geventwebsocket

from geventwebsocket.server import WebSocketServer
from flask_socketX import *
from concurrent.futures import ProcessPoolExecutor
import asyncio
from func import *
import json


app = Flask(__name__)
sockets = Sockets(app)
port=9001


@sockets.route('/my_ev/pak_hang_mak_ang')
def evcmd1(ws):
    # print('connected !!')
    wsX = ws
    if  request.environ.get('wsgi.websocket'):
        print('wsgi.webscoket activate!!')
    while not ws.closed:
        message = ws.receive()

        # ret = dataproc(message)
        print('recv : ',message)
        # print('send : ', ret)
        # ws = request.environ['wsgi.websocket']

        # data_json = json.dumps([1, 10, 'RemoteStopTransaction', {'transactionId': '0'}])
        # data_json = json.dumps([1, 10, 'RemoteStartTransaction', {'idTag': 'r00t1234567890!'}])
        ws.send('jyeahhh success connected!!')

@sockets.route('/')
def evcmd2(ws):
    # print('connected !!')
    while not ws.closed:
        message = ws.receive()

        dataproc(message,ws)
        # print(message)
        # ws.send(message)

@app.route('/')
def hello():
    return 'Welcome to flask socket!'


@app.route('/echo')
def echo():
    return 'hye bro!'

@app.route('/echo_test', methods=['GET'])
def echo_test():
    return render_template('echo_test.html')


@app.route('/my_ev',methods = ['POST'])
def api_datcmd():
    if request.headers['Content-Type'] == 'text/plain':
        return "Text Message: " + request.data

    elif request.headers['Content-Type'] == 'application/json':
        if request.is_json:
           cmd_req = request.json['cmd_req']
           idToken = request.json['idToken']
           print('> idToken < ',idToken)
           # print('> cmd_req < ', cmd_req)
           # global datalog_json
           # datalog_json.append(request.json)
           # data_json.append(request.json)
           print(request.json)
           ret = dataproc_cmd(cmd_req)
           ws = request.environ['wsgi.websocket']
           ws.send(ret)

           # if request.environ.get('wsgi.websocket'):
           #     ws = request.environ['wsgi.websocket']
           #     while True:
           #         message = ws.wait()
           #         print('result : ', ret)
           #         ws.send(ret)
           return '{"status":"1"}'

@app.route('/api')
def api():
    if request.environ.get('wsgi.websocket'):
        ws = request.environ['wsgi.websocket']
        while True:
            message = ws.wait()
            ws.send(message)
    return

async def startsock():
   # return app.run(host="0.0.0.0",port=port,debug=True)
   from gevent import pywsgi
   from geventwebsocket.handler import WebSocketHandler
   server = pywsgi.WSGIServer(('', port), app, handler_class=WebSocketHandler)
   # socketmiddle = SocketMiddleware(server,app,sockets)
   return server.serve_forever()

if __name__ == '__main__':

    print('--- ------------------------------ ')
    print('--(: P)- WBSCK--- ',port)
    print('--- ------------------------------ ')

    server = pywsgi.WSGIServer(('', port), app, handler_class=WebSocketHandler)
    # WebSocketServer(("", 8000), echo_app, debug=False).serve_forever()
    # socketmiddle = SocketMiddleware(server, app, sockets)
    server.serve_forever()

    # WebSocketServer(('0.0.0.0', 8000),Resource([('^/chat', ChatApplication),('^/.*', DebuggedApplication(flask_app))]),debug=False).serve_forever()

    # executor = ProcessPoolExecutor(1)
    # loop = asyncio.get_event_loop()
    # flsk = asyncio.ensure_future(loop.run_in_executor(executor, startsock))
    # loop.run_until_complete(startsock())
    # loop.run_forever()
    # startsock()