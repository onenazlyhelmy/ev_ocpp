import json
from flask import Flask,request
from flask_sockets import Sockets
from datetime import datetime

# app = Flask(__name__)
# sockets = Sockets(app)


def dataproc(dat):
    # print(dat)
    data = json.loads(dat)
    # data = dat
    x = 0

    try:

        for jsondat in data:
            x = x + 1
            # print(jsondat)
            if (jsondat == 'Authorize'):
                print(" >>>>  detect authorize, idtaginfo :{} ".format(data[x]))
                dd = json.dumps(data[x])
                d = json.loads(dd)
                # print(" idTag >> {} ".format(d['idTag']))
                return '{"current_time": "2019-08-29T07:03:40Z", "interval": 10,"status":"accepted"}'

            if (jsondat == 'BootNotification'):
                seq = data[0]
                hash = data[1]
                print(" >>>>  detect BootNotification, chargepoint : {},seq: {},hash: {} ".format(data[x],seq,hash))

                dd = json.dumps(data[x])
                d = json.loads(dd)
                print(" chargePointModel >> {} ".format(d['chargePointModel']))
                current_time =  datetime.strftime(datetime.now(),"%Y-%m-%dT%H:%M:%SZ")
                # print("now time : ",current_time)
                # await websocket.send('{"currentTime": "',datetime.utcnow().isoformat(),'", "interval": "10","status":"accepted"}')
                # wbsck.send('{"current_time": "2019-08-29T07:03:40Z", "interval": 10,"status":"accepted"}')
                data_json = json.dumps([seq+1,hash,{'currentTime':current_time,'interval':10,'status':'Accepted'}])
                return data_json
                # return ('{"current_time": "{}", "interval": 10,"status":"accepted"}'.format(current_time))

            if (jsondat == 'StatusNotification'):
                print(" >>>> detect StatusNotification, : {} ".format(data[x]))
                dd = json.dumps(data[x])
                d = json.loads(dd)
                seq = data[0]
                hash = data[1]
                data_json = json.dumps([seq + 1, hash, data[2]])
                print(" connectorId >> {},seq: {},hash: {} ".format(d['connectorId'],seq,hash))
                # wbsck.send('[2,"StatusNotification"]')
                return data_json

            if (jsondat == 'Heartbeat'):
                print(" >>>> detect Heartbeat, : {} ".format(data[x]))
                dd = json.dumps(data[x])
                d = json.loads(dd)
                seq = data[0]
                hash = data[1]
                current_time = datetime.strftime(datetime.now(), "%Y-%m-%dT%H:%M:%SZ")
                data_json = json.dumps([seq + 1, hash,{'currentTime':current_time}])
                print(" type >> {},seq: {},hash: {} ".format(data[2], seq, hash))
                # wbsck.send('[2,"StatusNotification"]')
                return data_json

    except Exception as s:
        print(s)

def dataproc_cmd(dat):
    print('process data :',dat)
    try:
        # dd = json.dumps(data[x])
        # d = json.loads(dd)
        # print(" idTag >> {} ".format(d['idTag']))
        if(dat == 'remotestart'):
            #data = json.loads(dat)
            print('remote start enable !!')
            data_json = json.dumps([1, 10, 'RemoteStartTransaction',{'idTag': 'r00t1234567890!'}])
            return data_json
    except Exception as s:
        print(s)