#!/usr/bin/env python



import asyncio
import websockets
import json
from funcflask import *
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import ProcessPoolExecutor
import _thread
import threading

from datetime import datetime
from flask import Flask,url_for,json,request
app = Flask(__name__)

port = 9001

async def on_receive(websocket, path):
    dat = await websocket.recv()
    # print(f"< {data}")
    print(dat)
    data  = json.loads(dat)
    x= 0

    try:

          for jsondat in data :
                x=x+1
                # print(jsondat)
                if(jsondat == 'Authorize'):
                    print(" >>>>  detect authorize, idtaginfo :{} ".format(data[x]))
                    dd = json.dumps(data[x])
                    d = json.loads(dd)
                    print(" idTag >> {} ".format(d['idTag']))

                if (jsondat == 'BootNotification'):
                    print(" >>>>  detect BootNotification, chargepoint : {} ".format(data[x]))
                    dd = json.dumps(data[x])
                    d = json.loads(dd)
                    print(" chargePointModel >> {} ".format(d['chargePointModel']))
                    # await websocket.send('{"currentTime": "',datetime.utcnow().isoformat(),'", "interval": "10","status":"accepted"}')
                    await websocket.send('{"current_time": "2019-08-29T07:03:40Z", "interval": 10,"status":"accepted"}')

                if (jsondat == 'StatusNotification'):
                    print(" >>>> detect StatusNotification, StatusNotification : {} ".format(data[x]))
                    dd = json.dumps(data[x])
                    d = json.loads(dd)
                    print(" connectorId >> {} ".format(d['connectorId']))
                    await websocket.send('[2,"StatusNotification"]')
    except Exception as s:
            print(s)

# @corotine
async def start_websock():
    print('*** start websocket port : {} ****'.format(port))
    wb= await websockets.serve(on_receive, "0.0.0.0", port)

    # wb = start_microweb()
    return wb
    # asyncio.ensure_future(start_server)
    # greeting = f"Hello {name}!"

    # await websocket.send(greeting)
    # print(f"> {greeting}")




#

# asyncio.get_event_loop().run_until_complete(fl)

# executor = ThreadPoolExecutor(max_workers=3)
# c = executor.submit(start_microweb())
# c = executor.submit(startwebsock())

# _thread.start_new_thread(startwebsock(),"")
# threads = []
# t = threading.Thread(target=startwebsock)
# # threads.append(t)
# t.start()
# _thread.start_new_thread(start_microweb,("Thread-2", 3, ))
if __name__ == "__main__":
    # execut = ThreadPoolExecutor(max_workers=3)
    # c = execut.submit(startwebsock())
    # b = asyncio.ensure_future(startwebsock)
    executor = ProcessPoolExecutor(1)
    loop = asyncio.get_event_loop()

    flsk = asyncio.ensure_future(loop.run_in_executor(executor, start_flaskweb))
    # flsk = asyncio.ensure_future(loop.run_in_executor(executor,websockets.serve(on_receive, "0.0.0.0", port)))
    # websock = asyncio.ensure_future(startwebsock())

    asyncio.get_event_loop().run_until_complete(start_websock())
    asyncio.get_event_loop().run_forever()

