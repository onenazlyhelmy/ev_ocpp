from flask import Flask,render_template
from flask_socketX import Sockets
from concurrent.futures import ProcessPoolExecutor
import asyncio


app = Flask(__name__)
sockets = Sockets(app)
port=9001

@sockets.route('/my_ev')
def evcmd1(ws):
    print('connected !!')
    while not ws.closed:
        message = ws.receive()
        print(message)
        # ws.send(message)

@app.route('/')
def hello():
    return 'Welcome to flask socket!'


@app.route('/echo')
def echo():
    return 'hye bro!'

@app.route('/echo_test', methods=['GET'])
def echo_test():
    return render_template('echo_test.html')

async def startsock():
   # return app.run(host="0.0.0.0",port=port,debug=True)
   from gevent import pywsgi
   from geventwebsocket.handler import WebSocketHandler
   server = pywsgi.WSGIServer(('', port), app, handler_class=WebSocketHandler)
   return server.serve_forever()

print('--- ------------------------------ ')
print('--(window-sux!)- WBSCK--- ',port)
print('--- ------------------------------ ')
# executor = ProcessPoolExecutor(1)
loop = asyncio.get_event_loop()
# flsk = asyncio.ensure_future(loop.run_in_executor(executor, startsock))
loop.run_until_complete(startsock())
# loop.run_forever()
# startsock()