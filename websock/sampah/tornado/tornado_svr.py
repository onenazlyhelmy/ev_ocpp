import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import uuid

clients = {}
cl_id=0
port=9090

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, application, request, **kwargs):
        super(WSHandler, self).__init__(application, request, **kwargs)

        # print('ip :',self.cl)
        # self.client_id = str(cl_id)
        # cl_id =cl_id +1
        # self.client_id = str(uuid.uuid4())

    def open(self):
        global cl_id

        # print(type(self.request))
        print(self.request)
        # x_real_ip = self.request.headers.get('X-Real-Ip', '')
        print('Client IP:' + self.request.remote_ip)
        if(self.request.remote_ip == '::1'):
            print('>>>>>> master connected ')
            self.client_id = 'master'
            print('connection for client {0} opened...'.format(self.client_id))
            clients[self.client_id] = self
        else:
            print('>>>>>> slaves connected ')
            self.client_id = str(cl_id)
            cl_id = cl_id + 1
            print('connection for client {0} opened...'.format(self.client_id))
            clients[self.client_id] = self

        # self.write_message("The server says: 'Hello'. Connection was accepted.")

    def on_message(self, message):
        self.write_message("The server says: " + message + " back at you")
        print('----- from client id :', self.client_id)
        if(self.client_id =='master'):
          print('received from master :', message)
        else:
          print('received from slave :', message)

    def on_close(self):
        clients.pop(self.client_id, None)
        print('connection closed...')

application = tornado.web.Application([
  (r'/ws', WSHandler),
  (r'/', MainHandler),
  (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])

if __name__ == "__main__":
  print('--tornado start -- on port:',port)
  application.listen(port)
  tornado.ioloop.IOLoop.instance().start()