from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api')
def api():
    if request.environ.get('wsgi.websocket'):
        ws = request.environ['wsgi.websocket']
        while True:
            # message = ws.wait()
            message = ws.receive()
            print('recv <<<  ',message)
            ws.send(message)
    return

@app.route('/my_ev/pak_hang_mak_ang')
def api2():
    if request.environ.get('wsgi.websocket'):
        ws = request.environ['wsgi.websocket']
        while True:
            # message = ws.wait()
            message = ws.receive()
            print('recv <<<  ',message)
            ws.send(message)
    return

if __name__ == '__main__':
    print('---- start wsgi server -----')
    http_server = WSGIServer(('',5000), app, handler_class=WebSocketHandler)
    http_server.serve_forever()