import asyncio
import websockets
import json

async def on_connect():
    # uri = "ws://localhost:9001/my_ev/pak_hang_mak_ang"
    uri = "ws://localhost:9090/ws"
    print('run wbclient : ',uri)
    async with websockets.connect(uri) as websocket:
        payload = json.dumps([2,'902f9c98-cd2f-43ea-8444-45edf35e6e2a','BootNotification',{'chargePointModel':'Optimus', 'chargePointVendor': 'The Mobility House'}])
        # '[2, "902f9c98-cd2f-43ea-8444-45edf35e6e2a", "BootNotification", {"chargePointModel": "Optimus", "chargePointVendor": "The Mobility House"}]'
        # await websocket.send(payload)
        data =  await websocket.recv()
        print(" recv <<<< : ",data)

asyncio.get_event_loop().run_until_complete(on_connect())
asyncio.get_event_loop().run_forever()