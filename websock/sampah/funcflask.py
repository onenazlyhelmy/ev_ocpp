from flask import Flask,url_for,json,request
app = Flask(__name__)
import json

@app.route('/ev_cmd',methods = ['POST'])
def api_datalog():
    if request.headers['Content-Type'] == 'text/plain':
        return "Text Message: " + request.data

    elif request.headers['Content-Type'] == 'application/json':
        if request.is_json:
           Plaza = request.json['Plaza']
           # global datalog_json
           # datalog_json.append(request.json)
           # data_json.append(request.json)
           print(request.json)

           return '{"status":"1"}'

def start_flaskweb():
   return app.run(host='0.0.0.0', threaded=False, debug=True)

