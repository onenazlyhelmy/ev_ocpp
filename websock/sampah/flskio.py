from flask import Flask,render_template
from flask_socketio import SocketIO


app= Flask(__name__)
app.config['SECRET_KEY'] = '4321'
socketio=SocketIO(app)

@app.route('/')
def session():
    return render_template('index.html')

def messageReceived(methods=['GET', 'POST']):
    print('msg recv')

@socketio.on('my event',namespace='ev_cmd')
def handle_my_ev(json,methods=['GET','POST']):
    print('recv evnt : '+str(json))
    socketio.emit('my response',json,callback=messageReceived)

@socketio.on(namespace='ev_cmd')
def handle_my_ev1(json,methods=['GET','POST']):
    print('recv evnt : '+str(json))
    socketio.emit('my response',json,callback=messageReceived)

if __name__ == '__main__':
   socketio.run(app,debug=True)